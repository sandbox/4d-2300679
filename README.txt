# Easy Hatena

The Easy Hatena modules is a small extension for Easy Social module
that provides a Hatena bookmark service widget.

## REQUIREMENTS

Easy Social
http://www.drupal.org/project/easy_social


## INSTALLATION

Install as usual, see http://drupal.org/node/895232
for further information.


## CONFIGURATION

- Go to /admin/config/content/easysocial and change it the way you want.

- Go to /admin/config/content/easy_social/widget-extra/hatena
  and select type of buttons.

- Go to Structure -> Content Types -> Type

- Enable Easy Social for the content types you want

- Also, go to Structure > Content Types > Type > Manage display

- Move the Easy Social widget around as you wish


## CONTACT

Satoru SHIKATA (4d) http://www.drupal.org/user/46980
